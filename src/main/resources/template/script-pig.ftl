<#-- @ftlvariable name="templateName" type="java.lang.String" -->
<#-- @ftlvariable name="outputFiles" type="java.lang.String" -->
<#-- @ftlvariable name="fileName" type="java.lang.String" -->
<#-- @ftlvariable name="filters" type="java.util.Set" -->
<#-- @ftlvariable name="filter" type="com.arkea.nv.test.log.modele.properties.Filter" -->
templateName: ${templateName}
outputFiles : ${outputFiles}
fileName : ${fileName}
filters :
<#list filters as filter>
    key : ${filter.key}
    value : ${filter.value}
    includeLeft : <#if filter.includeLeft>true<#else>false</#if>
    includeRight : <#if filter.includeRight>true<#else>false</#if>
</#list>