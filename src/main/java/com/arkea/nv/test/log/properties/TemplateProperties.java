package com.arkea.nv.test.log.properties;

import com.arkea.nv.test.log.modele.properties.Filter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Getter
@Setter
@ToString
@Configuration
@ConfigurationProperties(prefix = "template")
public class TemplateProperties {

    /**
     * templateName.
     */
    private String templateName;

    /**
     * outputFiles.
     */
    private String outputFiles;

    /**
     * outputFiles.
     */
    private String fileName;

    /**
     * filters.
     */
    private Set<Filter> filters;
}
