package com.arkea.nv.test.log.modele.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Filter {

    /**
     * key.
     */
    private String key;

    /**
     * value.
     */
    private String value;

    /**
     * includeLeft.
     */
    private Boolean includeLeft;

    /**
     * includeRight.
     */
    private Boolean includeRight;
}
