package com.arkea.nv.test.log;

import com.arkea.nv.test.log.exeption.FreeMarkerException;
import com.arkea.nv.test.log.properties.TemplateProperties;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

@Slf4j
@SpringBootApplication
public class LogApplication implements CommandLineRunner {

    /**
     * template.
     */
    private Template template;

    /**
     *
     */
    private TemplateProperties templateProperties;

    /**
     * Constructeur avec paramètres.
     *
     * @param template           template
     * @param templateProperties templateProperties
     */
    public LogApplication(Template template, TemplateProperties templateProperties) {
        this.template = template;
        this.templateProperties = templateProperties;
    }

    /**
     * Lancement de l'application
     *
     * @param args Agruments
     */
    public static void main(String[] args) {
        SpringApplication.run(LogApplication.class, args);
    }

    @Override
    public void run(String... args) {
        String path = String.format("%s/%s", templateProperties.getOutputFiles(), templateProperties.getFileName());
        try (Writer fileWriter = new FileWriter(path)) {
            // Création de la trace
            log.info(templateProperties.toString());
            template.process(templateProperties, fileWriter);
        } catch (TemplateException | IOException e) {
            throw new FreeMarkerException(e);
        }
    }
}
