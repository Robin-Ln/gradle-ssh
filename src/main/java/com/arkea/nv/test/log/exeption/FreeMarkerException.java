package com.arkea.nv.test.log.exeption;

public class FreeMarkerException extends RuntimeException {

    /**
     * Constructeur avec paramètre.
     *
     * @param throwable throwable
     */
    public FreeMarkerException(Throwable throwable) {
        super(throwable);
    }
}
