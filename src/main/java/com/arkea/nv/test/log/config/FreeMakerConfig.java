package com.arkea.nv.test.log.config;

import com.arkea.nv.test.log.exeption.FreeMarkerException;
import com.arkea.nv.test.log.properties.TemplateProperties;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@org.springframework.context.annotation.Configuration
public class FreeMakerConfig {
    /**
     * TEMPLATE_RESSOURCE.
     */
    private static final String TEMPLATE_RESSOURCE = "template/%s";

    /**
     * RESSOURCE_PATH.
     */
    private static final String RESSOURCE_PATH = "src/main/resources";

    /**
     * templateProperties
     */
    private TemplateProperties templateProperties;

    /**
     * Constructeur avec constructeur.
     *
     * @param templateProperties templateProperties
     */
    public FreeMakerConfig(TemplateProperties templateProperties) {
        this.templateProperties = templateProperties;
    }

    @Bean
    Template template() {
        try {
            Configuration configuration = createConfig();
            return configuration.getTemplate(String.format(TEMPLATE_RESSOURCE, templateProperties.getTemplateName()));
        } catch (IOException e) {
            throw new FreeMarkerException(e);
        }
    }

    /**
     * Configuration de free maker
     * @return Configuration
     * @throws IOException IOException
     */
    private Configuration createConfig() throws IOException {
        Configuration configuration = new Configuration();
        configuration.setDirectoryForTemplateLoading(new File(RESSOURCE_PATH));
        configuration.setDefaultEncoding(StandardCharsets.UTF_8.name());
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        return configuration;
    }
}
