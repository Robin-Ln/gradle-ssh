
| Taches | Description |
| --- | --- |
| clean | Supprime le dossier `build` |
| build | Copie le dossier `script` dans le dossier `build` |
| deploy | Copie le contenue du dossier `build` sur le serveur distant |

Exemple d'exécution : 
```
./gradlew deploy
```